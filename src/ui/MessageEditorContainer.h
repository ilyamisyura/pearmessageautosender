#ifndef MESSAGEEDITORCONTAINER_H
#define MESSAGEEDITORCONTAINER_H

#include <QWidget>
#include <QVBoxLayout>
#include "MessageEditor.h"
#include "data/Message.h"

class MessageEditorContainer : public QWidget
{
    Q_OBJECT

    QVBoxLayout* globalLayout;
    QVector<MessageEditor*> messageEditors;

public:
    explicit MessageEditorContainer(QWidget *parent = 0);

    QVector<Message> getMessagesList();

signals:

public slots:
    void setWidgetsNumber(int count);
};

#endif // MESSAGEEDITORCONTAINER_H
