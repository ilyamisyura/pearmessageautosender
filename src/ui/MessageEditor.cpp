#include "MessageEditor.h"
#include "ui_MessageEditor.h"

#include <QFileDialog>
#include <QGraphicsPixmapItem>
#include <QDebug>

MessageEditor::MessageEditor(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MessageEditor)
{
    ui->setupUi(this);
    ui->addImagePushButton->setVisible(false);
    ui->graphicsView->setVisible(false);
    ui->removeImageButton->setVisible(false);
}

MessageEditor::~MessageEditor()
{
    delete ui;

//    QObject::connect(ui->withImageCheckBox, SIGNAL(toggled(bool),this,SLOT(handleCheckBoxToggle(bool))
}

void MessageEditor::on_addImagePushButton_clicked()
{
    QString filePath = QFileDialog::getOpenFileName(this, tr("Select an image"),
                                  "../../", tr("Image files (*.bmp *.jpg *.jpeg *.gif *.png)"));

    if (filePath.size() > 0)
    {
        m_imageFilePath = filePath;
        QGraphicsScene* existingScene = ui->graphicsView->scene();
        if (existingScene)
        {
            delete existingScene;
        }
        QGraphicsScene* scene = new QGraphicsScene();
        ui->graphicsView->setScene(scene);
        QPixmap pixmap;
        pixmap.load(m_imageFilePath);
        QGraphicsPixmapItem* item = new QGraphicsPixmapItem(pixmap);
        scene->addItem(item);
        ui->graphicsView->fitInView(item,Qt::KeepAspectRatio);
    }
}

void MessageEditor::setNumber(int number)
{
    m_number = number;
    ui->numberLabel->setText(QString::number(number));
}

void MessageEditor::on_removeImageButton_clicked()
{
    m_imageFilePath = "";
    QGraphicsScene* scene = ui->graphicsView->scene();
    if (scene)
    {
        delete scene;
    }
}

void MessageEditor::resizeEvent(QResizeEvent *event)
{
    QGraphicsScene* scene = ui->graphicsView->scene();
    if (!scene)
    {
        return;
    }
    QList<QGraphicsItem*> items = ui->graphicsView->scene()->items();
    if (items.size()!=0)
    {
        QGraphicsItem* item = items.at(0);
        ui->graphicsView->fitInView(item,Qt::KeepAspectRatio);
    }
}

Message MessageEditor::getMessage()
{
    Message result;
    result.setText(ui->textEdit->toPlainText());
    if (m_imageFilePath.size() > 0)
    {
        result.setImageFilePath(m_imageFilePath);
    }

    return result;
}
