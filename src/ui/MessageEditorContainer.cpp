#include "MessageEditorContainer.h"

#include <QDebug>

MessageEditorContainer::MessageEditorContainer(QWidget *parent) : QWidget(parent)
{
    this->globalLayout = new QVBoxLayout(this);
    globalLayout->setObjectName(QStringLiteral("globalLayout"));
    globalLayout->setSizeConstraint(QLayout::SetMaximumSize);
}

void MessageEditorContainer::setWidgetsNumber(int count)
{
    count = (count < 1)? 1 : count;

    int currentWidgetsNumber = messageEditors.size();
    if (count > currentWidgetsNumber)
    {
        for (int i = currentWidgetsNumber; i < count; ++i)
        {
            MessageEditor* editor = new MessageEditor;
            editor->setNumber(i+1);
            globalLayout->addWidget(editor);
            messageEditors.append(editor);
        }
    }
    else if (count < currentWidgetsNumber)
    {
        for (int i = currentWidgetsNumber-1; i >= count; --i)
        {
            MessageEditor* currentEditor = messageEditors.at(i);
            delete currentEditor;
            messageEditors.removeAt(i);
        }
    }
}

QVector<Message> MessageEditorContainer::getMessagesList()
{
    QVector<Message> result;
    for (int i=0; i<messageEditors.size(); ++i)
    {
        Message message = messageEditors.at(i)->getMessage();
        result.push_back(message);
    }
    return result;
}
