#include "MainWindow.h"
#include "ui_MainWindow.h"

#include "data/Receiver.h"
#include "processing/QiMessageSender.h"
#include "processing/QAppleBasher.h"
#include "processing/QSystemProxySetupMachine.h"
#include "tools/SpinBoxDelegate.h"

#include <QProcess>
#include <QDebug>
#include <QNetworkInterface>
#include <QStringListModel>
#include <QDebug>
#include <QFileDialog>
#include <QListWidget>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->mainToolBar->setVisible(false);
    ui->statusBar->setVisible(false);
//    ui->stopDeliveryPushButton->setEnabled(false);

    //proxy stuff
    //interfaces
    p_interfacesListModel = new QStandardItemModel;
    ui->interfacesListView->setModel(p_interfacesListModel);
    //table view setup
    //headers
    p_proxyTableModel = new QStandardItemModel(0,4);
    QStandardItem* addressHeaderItem = new QStandardItem("Адрес");
    p_proxyTableModel->setHorizontalHeaderItem(0,addressHeaderItem);
    QStandardItem* portHeaderItem = new QStandardItem("Порт (0-65535)");
    p_proxyTableModel->setHorizontalHeaderItem(1,portHeaderItem);
    QStandardItem* proxyLoginHeaderItem = new QStandardItem("Логин");
    p_proxyTableModel->setHorizontalHeaderItem(2,proxyLoginHeaderItem);
    QStandardItem* proxyPasswordHeaderItem = new QStandardItem("Пароль");
    p_proxyTableModel->setHorizontalHeaderItem(3,proxyPasswordHeaderItem);
    //connecting to model
    ui->proxyFileTableView->setModel(p_proxyTableModel);
    //header stretch
    for (int i=0; i< p_proxyTableModel->columnCount(); ++i)
    {
        ui->proxyFileTableView->horizontalHeader()->setSectionResizeMode(i, QHeaderView::Stretch);
    }
    //adding spin box delegate to control port values
    SpinBoxDelegate* delegate = new SpinBoxDelegate(0,65535);
    ui->proxyFileTableView->setItemDelegateForColumn(1, delegate);

    loadInterfaces();
    //end proxy stuff

    //============================

    //receivers stuff
    //table view setup
    ui->receiversTableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    //headers
    p_receiversListModel = new QStandardItemModel(0,2);
    QStandardItem* contactHeaderItem = new QStandardItem("Получатель");
    p_receiversListModel->setHorizontalHeaderItem(0,contactHeaderItem);
    QStandardItem* hasAccountHeaderItem = new QStandardItem("Аккаунт в iMessage");
    p_receiversListModel->setHorizontalHeaderItem(1,hasAccountHeaderItem);
    //connecting to model
    ui->receiversTableView->setModel(p_receiversListModel);
    //header stretch
    ui->receiversTableView->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    ui->receiversTableView->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
    //end receivers stuff

    //============================

    //senders stuff
    //table view setup
    ui->sendersTableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    //headers
    p_sendersListModel = new QStandardItemModel(0,2);
    QStandardItem* senderIdHeaderItem = new QStandardItem("ID");
    p_sendersListModel->setHorizontalHeaderItem(0,senderIdHeaderItem);
    QStandardItem* senderPasswordHeaderItem = new QStandardItem("Пароль");
    p_sendersListModel->setHorizontalHeaderItem(1,senderPasswordHeaderItem);
    //connecting to model
    ui->sendersTableView->setModel(p_sendersListModel);
    //header stretch
    ui->sendersTableView->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    ui->sendersTableView->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
    //end receivers stuff

    //============================

    //sending stuff
    p_messageSender = new QiMessageSender;
    p_senderThread = new QThread;
    p_messageSender->setThread(p_senderThread);

    ui->changeAccountThresholdCheckBox->setChecked(false);
    ui->changeAccountThresholdLabel->setEnabled(false);
    ui->changeAccountThresholdSpinBox->setEnabled(false);
    QObject::connect(ui->changeAccountThresholdCheckBox, &QCheckBox::toggled, [=](const bool &checked)
    {
        ui->changeAccountThresholdLabel->setEnabled(checked);
        ui->changeAccountThresholdSpinBox->setEnabled(checked);
    });

    QObject::connect(ui->sendModeMultipleRadioButton, &QRadioButton::toggled, this, &MainWindow::setMessageNumbersBlockVisible);
    QObject::connect(ui->sendModeSingleRadioButton, &QRadioButton::toggled, [=]()
    {
        ui->messageEditorContainer->setWidgetsNumber(1);
        ui->messagesCountSpinBox->setValue(1);
    });

    QObject::connect(ui->messagesCountSpinBox, SIGNAL(valueChanged(int)), ui->messageEditorContainer, SLOT(setWidgetsNumber(int)));

    QObject::connect(p_messageSender,&QiMessageSender::messageSent,ui->messagesSentProgressBar, &QProgressBar::setValue);
    QObject::connect(p_messageSender,&QiMessageSender::messageSent, [=](const int &count)
    {
        ui->messagesSentValueLabel->setText(QString::number(count));
    });

    setMessageNumbersBlockVisible(false);
    ui->messageEditorContainer->setWidgetsNumber(1);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setMessageNumbersBlockVisible(bool visible)
{
    ui->messagesCountLabel->setVisible(visible);
    ui->messagesCountSpinBox->setVisible(visible);
}

void MainWindow::loadInterfaces()
{
    p_interfacesListModel->clear();

    QAppleBasher basher;
    basher.executeCommand("networksetup -listallnetworkservices");
    QString bashRes = basher.getLastResult();
    QStringList interfacesList = bashRes.split("\n",QString::SkipEmptyParts);
    interfacesList.removeAt(0);

    for (int i=0,count=0; i<interfacesList.size(); ++i)
    {
        //there's no ability to configure proxy settings for iPhone and Bluetooth
        QString currentInterface = interfacesList.at(i);
        if ( !( currentInterface.contains("bluetooth",Qt::CaseInsensitive) ) )
        {
            QStandardItem *interfaceItem = new QStandardItem(currentInterface);
            interfaceItem->setCheckable(true);
            interfaceItem->setCheckState(Qt::Unchecked);
            p_interfacesListModel->setItem(count, interfaceItem);
            count++;
        }
    }
}

void MainWindow::on_startDeliveryButton_clicked()
{

    QVector<Message> messages = ui->messageEditorContainer->getMessagesList();
    ReceiversList receivers = m_currentReceiversList.getReceiversWithAccount();
    if (receivers.size() < 1)
    {
        QMessageBox messageBox;
        messageBox.setText("No receivers available");
        messageBox.exec();
        return;
    }

    ui->messagesSentProgressBar->setMaximum(messages.size()*receivers.size());
    ui->messagesSentProgressBar->setValue(0);

    p_messageSender->setMessagesQueue(messages);
    p_messageSender->setReceiversList(receivers);
    p_messageSender->setSendersList(m_currentSendersList);
    p_messageSender->setMessagesDelayInterval(ui->messagesDelaySpinBox->value());
    if (ui->changeAccountThresholdCheckBox->isChecked())
    {
        p_messageSender->setChangeAccountThreshold(ui->changeAccountThresholdSpinBox->value());
    }

    p_senderThread->start();
}

void MainWindow::on_chooseProxyFilePushButton_clicked()
{
    m_currentProxyFilePath = QFileDialog::getOpenFileName(this,
            tr("Open file with proxy settings"), "/", tr("Text files (*.txt);;All files (*.*)"));

    if (m_currentProxyFilePath.size()!=0)
    {
        ui->proxyFilePathLineEdit->setText(m_currentProxyFilePath);
        on_loadProxyFilePushButton_clicked();
    }
}

void MainWindow::on_setUpProxyPushButton_clicked()
{
    QSystemProxySetupMachine proxySetupper;
    QMessageBox messageBox;

    QModelIndexList selectedRows = ui->proxyFileTableView->selectionModel()->selectedRows();
    quint16 selectedRowsCount = selectedRows.size();
    if (selectedRowsCount != 1)
    {
        messageBox.setText(QString("Select 1 whole row to set interface proxy settings.\n\nNow %1 whole rows selected").arg(selectedRowsCount));
        messageBox.exec();
        return;
    }

    bool allowedToSet = true;
    for (int j=0; j < 2; ++j)
    {
        QString currentItemText = p_proxyTableModel->item(selectedRows.at(0).row(),j)->text();
        if (currentItemText.trimmed().size()==0)
        {
            allowedToSet = false;
        }
    }
    QString usernameItemText = p_proxyTableModel->item(selectedRows.at(0).row(),2)->text();
    QString passwordItemText = p_proxyTableModel->item(selectedRows.at(0).row(),3)->text();

    if (
            ( (usernameItemText.trimmed().size() != 0) && (passwordItemText.trimmed().size() == 0) )
         || ( (usernameItemText.trimmed().size() == 0) && (passwordItemText.trimmed().size() != 0) )
        )
    {
        allowedToSet = false;
    }

    if (!allowedToSet)
    {
        messageBox.setText(QString("Fix empty fields!\nIf you filled one of fields \"Username\" or \"Password\",\n"
                                   "you have to fill anoter one, so both of them should not be empty!"));
        messageBox.exec();
        return;
    }

    ProxyInfo proxyInfo(
                    p_proxyTableModel->item(selectedRows.at(0).row(),0)->text(),
                    p_proxyTableModel->item(selectedRows.at(0).row(),1)->text().toInt(),
                    p_proxyTableModel->item(selectedRows.at(0).row(),2)->text(),
                    p_proxyTableModel->item(selectedRows.at(0).row(),3)->text()
                );

    QList<QStandardItem*> selectedInterfacesItems;
    for (int i=0; i<p_interfacesListModel->rowCount(); ++i)
    {
        QStandardItem *item = p_interfacesListModel->item(i);
        if (item->checkState() == Qt::Checked)
        {
            selectedInterfacesItems.append(item);
        }
    }

    if (selectedInterfacesItems.size() == 0)
    {
        messageBox.setText("Select at least 1 interface to set proxy settings.");
        messageBox.exec();
        return;
    }

    for (int i=0; i<selectedInterfacesItems.size(); ++i)
    {
        proxySetupper.setUpProxy(selectedInterfacesItems.at(i)->text(),proxyInfo);
    }

    messageBox.setText("Настройка прокси прошла успешно!");
    messageBox.exec();
}

void MainWindow::on_loadProxyFilePushButton_clicked()
{
    m_currentProxyFilePath = ui->proxyFilePathLineEdit->text();

    p_proxyTableModel->removeRows(0,p_proxyTableModel->rowCount());

    ProxyInfoList proxyInfoList;
    proxyInfoList.loadFromFile(ui->proxyFilePathLineEdit->text());

    for (int i=0; i<proxyInfoList.size(); ++i)
    {
        p_proxyTableModel->insertRow(i);

        ProxyInfo* proxyInfo = proxyInfoList.at(i);

        QModelIndex addressIndex = p_proxyTableModel->index(i,0);
        p_proxyTableModel->setData(addressIndex,QVariant(proxyInfo->address()));

        QModelIndex portIndex = p_proxyTableModel->index(i,1);
        p_proxyTableModel->setData(portIndex,QVariant(proxyInfo->port()));

        QModelIndex usernameIndex = p_proxyTableModel->index(i,2);
        p_proxyTableModel->setData(usernameIndex,QVariant(proxyInfo->username()));

        QModelIndex passwordIndex = p_proxyTableModel->index(i,3);
        p_proxyTableModel->setData(passwordIndex,QVariant(proxyInfo->password()));
    }
}

void MainWindow::on_setProxyFilePushButton_clicked()
{
    bool allowedToWrite = true;
    for (int i=0; i < p_proxyTableModel->rowCount(); ++i)
    {
        for (int j=0; j < 2; ++j)
        {
            QString currentItemText = p_proxyTableModel->item(i,j)->text();
            if (currentItemText.trimmed().size()==0)
            {
                allowedToWrite = false;
                QModelIndex index = p_interfacesListModel->index(i,j);
                p_proxyTableModel->setData(index, QBrush(Qt::red), Qt::ForegroundRole);
            }
        }
        QString usernameItemText = p_proxyTableModel->item(i,2)->text();
        QString passwordItemText = p_proxyTableModel->item(i,3)->text();

        if (
                ( (usernameItemText.trimmed().size() != 0) && (passwordItemText.trimmed().size() == 0) )
             || ( (usernameItemText.trimmed().size() == 0) && (passwordItemText.trimmed().size() != 0) )
            )
        {
            allowedToWrite = false;
        }

    }

    if (allowedToWrite)
    {
        QFile file(m_currentProxyFilePath);
        if (!file.open(QIODevice::ReadWrite | QIODevice::Text | QIODevice::Truncate))
        {
            QMessageBox messageBox;
            messageBox.setText("Cannot open file " + m_currentProxyFilePath);
            messageBox.exec();
            return;
        }

        QTextStream out(&file);
        for (int i=0; i < p_proxyTableModel->rowCount(); ++i)
        {
            out << p_proxyTableModel->item(i,0)->text()
                << ":"
                << p_proxyTableModel->item(i,1)->text();

            if (    (p_proxyTableModel->item(i,2)->text().trimmed().size()!=0)
                 && (p_proxyTableModel->item(i,3)->text().trimmed().size()!=0) )
            {
                out << ":"
                    << p_proxyTableModel->item(i,2)->text()
                    << ":"
                    << p_proxyTableModel->item(i,3)->text();
            }
            out << "\n";
        }

    }
    else
    {
        QMessageBox messageBox;
        messageBox.setText("Incorrect data. Fix empty fields");
        messageBox.exec();
    }
}

void MainWindow::on_addProxySettingsButton_clicked()
{
    int newRowIndex = p_proxyTableModel->rowCount();
    p_proxyTableModel->insertRow(newRowIndex);
    p_proxyTableModel->setItem(newRowIndex,0,new QStandardItem(""));
    p_proxyTableModel->setItem(newRowIndex,1,new QStandardItem(""));
}

void MainWindow::on_chooseReceiversFilePushButton_clicked()
{
    m_currentReceiversFilePath = QFileDialog::getOpenFileName(this,
            tr("Open file with receivers"), "/", tr("Text files (*.txt);;All files (*.*)"));

    if (m_currentReceiversFilePath.size()!=0)
    {
        ui->receiversFilePathLineEdit->setText(m_currentReceiversFilePath);
        on_loadReceiversFilePushButton_clicked();
    }
}

void MainWindow::on_loadReceiversFilePushButton_clicked()
{
    m_currentReceiversFilePath = ui->receiversFilePathLineEdit->text();
    m_currentReceiversList.loadFromFile(this->m_currentReceiversFilePath);

    p_receiversListModel->removeRows(0,p_receiversListModel->rowCount());

    redrawReceiversHasAccountState();
}

void MainWindow::on_checkReceiversPushButton_clicked()
{
    m_currentReceiversList.checkAllReceivers();
    redrawReceiversHasAccountState();
}

void MainWindow::redrawReceiversHasAccountState()
{
    p_receiversListModel->removeRows(0,p_receiversListModel->rowCount());

    for (int i=0; i<m_currentReceiversList.size(); ++i)
    {
        p_receiversListModel->insertRow(i);

        Receiver* currentReceiver = m_currentReceiversList.at(i);

        QModelIndex idIndex = p_receiversListModel->index(i,0);
        p_receiversListModel->setData(idIndex,QVariant::fromValue(currentReceiver));
        p_receiversListModel->itemFromIndex(idIndex)->setText(currentReceiver->id());

        QModelIndex hasAccountIndex = p_receiversListModel->index(i,1);
        p_receiversListModel->setData(hasAccountIndex,QVariant::fromValue(currentReceiver));

        QString accountState;
        if (!currentReceiver->everBeenChecked())
        {
            accountState = "Не проверено";
        }
        else if (!currentReceiver->getLastCheckSuccess())
        {
            accountState = "Ошибка при проверке";
        }
        else
        {
            accountState = currentReceiver->hasiMessageAccount()? "Есть" : "Нет";
        }
        p_receiversListModel->itemFromIndex(hasAccountIndex)->setText(accountState);
    }
}

void MainWindow::on_chooseSendersFilePushButton_clicked()
{
    m_currentSendersFilePath = QFileDialog::getOpenFileName(this,
            tr("Open file with receivers"), "/", tr("Text files (*.txt);;All files (*.*)"));

    if (m_currentSendersFilePath.size()!=0)
    {
        ui->sendersFilePathLineEdit->setText(m_currentSendersFilePath);
        on_loadSendersFilePushButton_clicked();
    }
}

void MainWindow::on_loadSendersFilePushButton_clicked()
{
    m_currentSendersList.clear();

    m_currentSendersFilePath = ui->sendersFilePathLineEdit->text();
    m_currentSendersList.loadFromFile(this->m_currentSendersFilePath);

    p_sendersListModel->removeRows(0,p_sendersListModel->rowCount());

    for (int i=0; i<m_currentSendersList.size(); ++i)
    {
        p_sendersListModel->insertRow(i);

        SenderAccount* currentSender = m_currentSendersList.at(i);

        QModelIndex idIndex = p_sendersListModel->index(i,0);
        p_sendersListModel->setData(idIndex,QVariant::fromValue(currentSender));
        p_sendersListModel->itemFromIndex(idIndex)->setText(currentSender->id());

        QModelIndex passwordIndex = p_sendersListModel->index(i,1);
        p_sendersListModel->setData(passwordIndex,QVariant::fromValue(currentSender));
        p_sendersListModel->itemFromIndex(passwordIndex)->setText(currentSender->password());
    }
}

void MainWindow::on_deleteProxySettingsButton_clicked()
{
    QModelIndexList selectedRows = ui->proxyFileTableView->selectionModel()->selectedRows();
    quint16 selectedRowsCount = selectedRows.size();
    if (selectedRowsCount < 1)
    {
        QMessageBox messageBox;
        messageBox.setText(QString("Select at least 1 whole row to delete.\n\nNo rows selected."));
        messageBox.exec();
        return;
    }

    int deletedCount = 0;
    for (int i=0; i<selectedRowsCount; ++i)
    {
        QModelIndex currentIndex = selectedRows.at(i);
        p_proxyTableModel->removeRow(currentIndex.row()-deletedCount);
        ++deletedCount;
    }
}

void MainWindow::on_stopDeliveryPushButton_clicked()
{
//    ui->messagesSentProgressBar->setValue(0);
//    ui->messagesSentValueLabel->setText("");

    if (p_senderThread->isRunning())
    {
        p_senderThread->exit();

        QMessageBox messageBox;
        messageBox.setText("Sending was stopped.");
        messageBox.exec();
    }
//    p_messageSender->forceStopSending();
}

void MainWindow::on_exportReceiversWithAccountsPushButton_clicked()
{
    ReceiversList activeList = m_currentReceiversList.getReceiversWithAccount();
    if (activeList.size() < 1)
    {
        QMessageBox messageBox;
        messageBox.setText("No active receivers found!");
        messageBox.exec();
        return;
    }

    QString fileName = QFileDialog::getSaveFileName(this,
            tr("Save receivers"), "/",
            tr("Text document (*.txt)"));
    qDebug() << fileName;


    activeList.saveToFile(fileName);


//    m_currentReceiversList
}

void MainWindow::on_checkBox_toggled(bool checked)
{


    for (int i=0; i<m_currentReceiversList.size(); ++i)
    {
        Receiver* currentReceiver = m_currentReceiversList.at(i);
        if (checked)
        {
            currentReceiver->setForceHasAccount();
        }
        else
        {
            currentReceiver->setNeedsCheck();
        }
    }
    redrawReceiversHasAccountState();

}
