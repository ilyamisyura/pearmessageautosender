#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStandardItemModel>
#include <QThread>

#include "data/ReceiversList.h"
#include "data/SenderAccountsList.h"
#include "processing/QiMessageSender.h"
#include "MessageEditor.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    //proxy stuff
    QStandardItemModel* p_proxyTableModel;
    QStandardItemModel* p_interfacesListModel;
    QString m_currentProxyFilePath;

    //receivers stuff
    QString m_currentReceiversFilePath;
    QStandardItemModel* p_receiversListModel;
    ReceiversList m_currentReceiversList;
    void redrawReceiversHasAccountState();

    //senders stuff
    QString m_currentSendersFilePath;
    QStandardItemModel* p_sendersListModel;
    SenderAccountsList m_currentSendersList;
    QiMessageSender* p_messageSender;
    QThread* p_senderThread;

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    //proxy
    void loadInterfaces();

    //
    void setMessageNumbersBlockVisible(bool visible);


    //auto
    void on_startDeliveryButton_clicked();

    void on_chooseProxyFilePushButton_clicked();

    void on_loadProxyFilePushButton_clicked();

    void on_setProxyFilePushButton_clicked();

    void on_setUpProxyPushButton_clicked();

    void on_addProxySettingsButton_clicked();

    void on_chooseReceiversFilePushButton_clicked();

    void on_loadReceiversFilePushButton_clicked();

    void on_checkReceiversPushButton_clicked();

    void on_chooseSendersFilePushButton_clicked();

    void on_loadSendersFilePushButton_clicked();

    void on_deleteProxySettingsButton_clicked();

    void on_stopDeliveryPushButton_clicked();

    void on_exportReceiversWithAccountsPushButton_clicked();

    void on_checkBox_toggled(bool checked);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
