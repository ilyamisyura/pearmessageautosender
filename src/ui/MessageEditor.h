#ifndef MESSAGEEDITOR_H
#define MESSAGEEDITOR_H

#include <QWidget>
#include "data/Message.h"

namespace Ui {
class MessageEditor;
}

class MessageEditor : public QWidget
{
    Q_OBJECT

    int m_number;
    QString m_imageFilePath;

public:
    explicit MessageEditor(QWidget *parent = 0);
    ~MessageEditor();

    void setNumber(int number);

    Message getMessage();

private slots:
    void on_addImagePushButton_clicked();

    void on_removeImageButton_clicked();

protected:
    void resizeEvent(QResizeEvent *event);

private:
    Ui::MessageEditor *ui;
};

#endif // MESSAGEEDITOR_H
