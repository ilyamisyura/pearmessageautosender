#include "ui/MainWindow.h"
#include <QApplication>
#include <QProcess>
#include <QDebug>
#include <QTime>
#include <random>

#include "processing/QAppleScripter.h"

int main(int argc, char *argv[])
{
    QTime t = QTime::currentTime();
    qsrand((uint)t.msec());

//    QString script = "tell application"
//    QAppleScripter scripter;
//    scripter.runAppleScript(script);

//    return 1;

    QApplication a(argc, argv);
    a.setApplicationName("iMessage Sender");
    a.setApplicationDisplayName("iMessage Sender");
    MainWindow w;
    w.show();

    return a.exec();
}
