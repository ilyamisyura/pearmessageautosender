#ifndef QSYSTEMPROXYSETUPMACHINE_H
#define QSYSTEMPROXYSETUPMACHINE_H

#include <QObject>
#include <QFile>

#include "data/ProxyInfoList.h"

class QSystemProxySetupMachine : public QObject
{
    Q_OBJECT
public:
    enum ProxyType {
        HTTP,
        HTTPS,
        FTP,
        SOCKS,
        RTSP,
        GOPHER
    };

    explicit QSystemProxySetupMachine(QObject *parent = 0);

//    ProxyInfoList parseProxySettingsFile(QString filePath);

    void setUpProxy(QString interface, QString host, quint16 port, QString username = "", QString password = "");
    void setUpProxy(QString interface, ProxyInfo proxyInfo);

    QString getLastMessage;

signals:

public slots:
};

#endif // QSYSTEMPROXYSETUPMACHINE_H
