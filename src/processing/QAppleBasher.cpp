#include "QAppleBasher.h"

#include <QProcess>
#include <QThread>

QAppleBasher::QAppleBasher(QObject *parent) : QObject(parent)
{

}

void QAppleBasher::executeCommand(QString command, quint32 timeoutMsecs)
{
    QProcess proc;

    QStringList args;
    args << "-c" << command;
    proc.start( "/bin/bash", args);
    proc.waitForFinished(timeoutMsecs);
    m_lastResult = proc.readAll();

    emit commandExecuted();
}

void QAppleBasher::executeCommand(QStringList args, quint32 timeoutMsecs)
{
    QProcess proc;

    proc.start( "/bin/bash", args);
    proc.waitForFinished(timeoutMsecs);
    m_lastResult = proc.readAll();

    emit commandExecuted();
}
