#ifndef QAPPLEBASHER_H
#define QAPPLEBASHER_H

#include <QObject>

class QAppleBasher : public QObject
{
    Q_OBJECT

    QString m_lastResult;

public:
    explicit QAppleBasher(QObject *parent = 0);

    QString getLastResult() { return m_lastResult; }

signals:
    void commandExecuted();

public slots:
    void executeCommand(QString command, quint32 timeoutMsecs = 30000);
    void executeCommand(QStringList params, quint32 timeoutMsecs = 30000);
};

#endif // QAPPLEBASHER_H
