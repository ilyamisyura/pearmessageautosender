#include "QiMessageSender.h"

#include "QAppleScripter.h"

#include <QFile>
#include <QDebug>
#include <QTimer>
#include <QEventLoop>
#include <QThread>
#include <QObject>
#include <QMessageBox>

const quint32 QiMessageSender::c_timerPeriodMsecs = 1000;

QiMessageSender::QiMessageSender(QObject *parent) : QObject(parent)
{
    pSetDefaults();

    p_timer = new QTimer;
    QObject::connect(p_timer, &QTimer::timeout, this, &QiMessageSender::nextSendIteration);
}

void QiMessageSender::sendMessage(Receiver receiver, Message message)
{
    pSendMessage(receiver.id(),message.getText(),message.getImageFilePath());
}

void QiMessageSender::pSendMessage(QString receiver, QString message, QString attachmentFilePath)
{
    QAppleScripter scripter;

    QString script = pBuildSendScript(receiver, message, attachmentFilePath);

    scripter.runAppleScript(script);
}

QString QiMessageSender::pBuildSendScript(QString receiver, QString message, QString attachmentFilePath)
{
    QString script =
            "tell application \"Messages\"\n"
                "set targetBuddy to \"" + receiver + "\"\n"
                "set targetService to id of 1st service whose service type = iMessage\n"
                "set textMessage to \"" + message + "\"\n"
                "set theBuddy to buddy targetBuddy of service id targetService\n";
    bool sendAttachmentFlag = false;
    if (attachmentFilePath.size()!=0)
    {
        QFile attachmentFile(attachmentFilePath);
        if (attachmentFile.exists())
        {
            script+= "set targetFileName to POSIX file \"" + attachmentFilePath + "\"\n";
            sendAttachmentFlag = true;
        }
    }

    if (sendAttachmentFlag)
    {
        script+="send targetFileName to theBuddy\n";
    }
    script+= "send textMessage to theBuddy\n";
    script+="end tell\n";

    return script;
}

QString QiMessageSender::logInMessages(SenderAccount account, quint32 waitForLoginSeconds)
{
    QString logInScript =
            "activate application \"Messages\"\n"
            "delay 0.5\n"

            "tell application \"System Events\" to tell process \"Messages\"\n"
                "click menu item 3 of menu 2 of menu bar 1\n"
                "delay 0.5\n"
                "click button 2 of toolbar 1 of front window\n"
                "delay 0.5\n"
                "click button 2 of toolbar 1 of front window\n"
                "delay 0.5\n"
                "click text field 1 of group 1 of group 1 of front window\n"
                "delay 0.5\n"
                "keystroke \"a\" using command down\n"
                "delay 0.5\n"
                "keystroke \n" + account.id() + "\"\n"
                "delay 0.5\n"
                "keystroke tab\n"
                "delay 0.5\n"
                "keystroke \n" + account.password() + "\"\n"
                "click button 2 of group 1 of group 1 of front window\n"
                "set exitString to ""\n"
                "set tries to 0\n"
                "set maxTries to " + QString::number(waitForLoginSeconds) +"\n"
                "repeat while ((exitString = "") or (tries < maxTries))\n"
                    "tell busy indicator 1 of group 1 of group 1 of front window\n"
                        "if not (exists) then\n"
                            "set exitString to \"exit\"\n"
                        "end if\n"
                    "end tell\n"
                "end repeat\n"
                "if ( (tries = maxTries) and (exitString = "") )\n"
                    "set result to \"error: login timeout\"\n"
                "end if\n"

                "click button 1 of front window\n"

            "end tell\n"
            "result\n";

    QAppleScripter scripter;
    scripter.runAppleScript(logInScript);

    return scripter.getLastResult();
}

void QiMessageSender::nextSendIteration()
{
    Message currentMessage = m_currentMessageQueue.at(m_currentMessageIndex);
    Receiver currentReceiver = *m_currentReceiversList.at(m_currentReceiverIndex);
    sendMessage(currentReceiver,currentMessage);
    qDebug() << "Message" << m_currentMessageIndex << "to receiver" << m_currentReceiverIndex;

    ++m_currentReceiverIndex;
    ++m_messagesSentCount;

    if (!m_forceFinished)
    {
        emit messageSent(m_messagesSentCount);

        if ( (m_senderAccountsList.size() > 0)
             && (m_changeAccountThreshold > 0)
             && ((m_messagesSentCount % m_changeAccountThreshold) == 0) )
        {
            p_timer->stop();
            qDebug() << "relogin";
            m_senderAccountsList.logInRandomAccount();
            bool loggedIn = false;
            QAppleScripter scripter;
            int tries = 0;
            while ((!loggedIn)&&(tries<30))
            {
                scripter.runAppleScript("tell application \"Messages\" to set accountEnabled to enabled of 1st service whose service type = iMessage");
                QString res = scripter.getLastResult();
                qDebug() << res;
                loggedIn = res.contains("true",Qt::CaseInsensitive);
                tries++;
                QThread::sleep(2);
            }
            if (loggedIn)
            {
                p_timer->start();
            }
            else
            {
                forceStopSending();
                QMessageBox messageBox;
                messageBox.setText("Error occured while logging in! Aborting sending");
                messageBox.exec();
                return;
            }
        }
    }
    else
    {
        qDebug() << "FORCE STOPPED";
    }


    if (m_currentReceiverIndex == m_currentReceiversList.size())
    {
        qDebug() << "TO NEXT MESSAGE";
        m_currentReceiverIndex = 0;
        ++m_currentMessageIndex;
        QThread::sleep(m_messagesDelaySeconds);
    }

    if (m_currentMessageIndex == m_currentMessageQueue.size())
    {
        qDebug() << "STOP SENDING!";
        forceStopSending();
    }
}

void QiMessageSender::startSending()
{
    m_forceFinished = false;
    qDebug() << "Starting sending with parameters:";
    qDebug() << "Change account threshold" << m_changeAccountThreshold;

    p_timer->start(100);
}

void QiMessageSender::forceStopSending()
{
    m_forceFinished = true;
    p_parentThread->exit();

    m_currentReceiverIndex = 0;
    m_currentMessageIndex = 0;
    m_messagesSentCount = 0;
}

void QiMessageSender::pauseSending()
{
    p_parentThread->exit();
}

void QiMessageSender::continueSending()
{
    p_timer->start();
}

void QiMessageSender::pSetDefaults()
{
    m_currentReceiverIndex = 0;
    m_currentMessageIndex = 0;
    m_messagesSentCount = 0;
    m_changeAccountThreshold = 0;
    m_forceFinished = false;
}

void QiMessageSender::clearAll()
{
    m_senderAccountsList.deleteAllAndClear();
    m_currentMessageQueue.clear();
    m_currentReceiversList.deleteAllAndClear();
}

void QiMessageSender::setThread(QThread *thread)
{
    p_parentThread = thread;
    p_timer->moveToThread(p_parentThread);
    this->moveToThread(p_parentThread);
    QObject::connect(p_parentThread, &QThread::started, this, &QiMessageSender::startSending);
    QObject::connect(p_parentThread, &QThread::finished, this, &QiMessageSender::forceStopSending);
    QObject::connect(p_parentThread, &QThread::finished, p_timer, &QTimer::stop);
}
