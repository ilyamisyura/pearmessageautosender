#include "QAppleScripter.h"
#include <QProcess>
#include <QDebug>

QAppleScripter::QAppleScripter(QObject *parent) : QObject(parent)
{

}

bool QAppleScripter::runAppleScript(QString script)
{
    bool res = true;

    QString osascript = "/usr/bin/osascript";
    QStringList processArguments;
    processArguments << "-l" << "AppleScript";

    QProcess p;
    p.start(osascript, processArguments);
    p.write(script.toUtf8());
    p.closeWriteChannel();
    res = p.waitForFinished();

    m_lastResult = p.readAll();

    return res;
}
