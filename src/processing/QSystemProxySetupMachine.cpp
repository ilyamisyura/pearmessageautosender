#include "QSystemProxySetupMachine.h"

#include <QProcess>
#include <QDebug>
#include <QMessageBox>

QSystemProxySetupMachine::QSystemProxySetupMachine(QObject *parent) : QObject(parent)
{

}

void QSystemProxySetupMachine::setUpProxy(QString interface, ProxyInfo proxyInfo)
{
    setUpProxy(
                   interface,
                   proxyInfo.address(),
                   proxyInfo.port(),
                   proxyInfo.username(),
                   proxyInfo.password()
               );
}

void QSystemProxySetupMachine::setUpProxy(QString interface, QString host, quint16 port, QString username, QString password)
{
    QProcess proc;

    QStringList args;
    QString command = QString("sudo networksetup -setwebproxy \"%1\" %2 %3").arg(interface).arg(host).arg(port);
    if ( (username.trimmed().size() > 0) && (password.trimmed().size() > 0) )
    {
        command.append(QString(" on %1 %2").arg(username.trimmed()).arg(password.trimmed()));
    }
    args << "-c" << command;
    proc.start( "/bin/bash", args );
    proc.waitForFinished();

    QString uID = proc.readAll();
}

//ProxyInfoList QSystemProxySetupMachine::parseProxySettingsFile(QString filePath)
//{
//    QMessageBox messageBox;
//    QFile file;
//    file.setFileName(filePath);

//    ProxyInfoList result;

//    if (!file.exists())
//    {
//        messageBox.setText("No such file " + file.fileName());
//        messageBox.exec();
//    }
//    else
//    {
//        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
//        {
//            messageBox.setText("Cannot open file " + file.fileName());
//            messageBox.exec();
//        }
//        else
//        {
//            QTextStream in(&file);
//            while (!in.atEnd()) {
//                QString line = in.readLine();
//                QStringList proxyLine = line.split(':',QString::SkipEmptyParts);
//                if (proxyLine.size()!=0)
//                {
//                    result.insert(proxyLine[0], (proxyLine.size() > 1)? proxyLine[1].toInt() : 0);

//                }
//            }
//        }
//    }

//    return result;
//}
