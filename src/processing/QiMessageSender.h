#ifndef QIMESSAGESENDER_H
#define QIMESSAGESENDER_H

#include <QObject>
#include <QTimer>
#include <QVector>
#include <QThread>

#include "data/Message.h"
#include "data/ReceiversList.h"
#include "data/SenderAccountsList.h"

class QiMessageSender : public QObject
{
    Q_OBJECT

    QString pBuildSendScript(QString receiver,QString message, QString attachmentFilePath = "");
    void pSendMessage(QString receiver,QString message, QString attachmentFilePath = "");
    void pSetDefaults();

    static const quint32 c_timerPeriodMsecs;

    QTimer* p_timer;
    QThread* p_parentThread;
    SenderAccountsList m_senderAccountsList;
    QVector<Message> m_currentMessageQueue;
    ReceiversList m_currentReceiversList;

    quint32 m_changeAccountThreshold;
    quint32 m_messagesDelaySeconds;
    quint32 m_currentReceiverIndex;
    quint32 m_currentMessageIndex;

    quint32 m_messagesSentCount;

    bool m_forceFinished;

//    QMetaObject::Connection* m_con_sendMessage;

public:
    explicit QiMessageSender(QObject *parent = 0);

    QString logInMessages(SenderAccount account, quint32 waitForLoginSeconds);
//    QString getCurrentAccountName();

//    void sendMessages(ReceiversList receiver, QVector<Message> messagesVector, quint32 delayBetweenMessagesMSecs = 0);
//    void sendMessages(Receiver receiver, QVector<Message> messagesVector, quint32 delayBetweenMessagesMSecs = 0);
    void sendMessage(Receiver receiver, Message message);

    void setMessagesQueue(QVector<Message> messagesVector) { m_currentMessageQueue = messagesVector; }
    void setChangeAccountThreshold(quint32 changeAccountThreshold) { m_changeAccountThreshold = changeAccountThreshold; }
    void setReceiversList(ReceiversList receivers) { m_currentReceiversList = receivers; }
    void setSendersList(SenderAccountsList senders) { m_senderAccountsList = senders; }
    void setMessagesDelayInterval(quint32 seconds) { m_messagesDelaySeconds = seconds; }
    void setThread(QThread* thread);

signals:
    void sendingStarted();
    void sendingFinished();
    void senderChangeNeeded();
    void messageSent(int count);

private slots:
    void nextSendIteration();

public slots:
    void startSending();
    void pauseSending();
    void continueSending();
    void forceStopSending();
    void clearAll();
};

#endif // QIMESSAGESENDER_H
