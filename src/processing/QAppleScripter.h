#ifndef QAPPLESCRIPTER_H
#define QAPPLESCRIPTER_H

#include <QObject>

class QAppleScripter : public QObject
{
    Q_OBJECT

    QString m_lastResult;
public:
    explicit QAppleScripter(QObject *parent = 0);
    QString getLastResult() { return m_lastResult; }

signals:

public slots:
    bool runAppleScript(QString script);
};

#endif // QAPPLESCRIPTER_H
