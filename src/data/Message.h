#ifndef MESSAGE_H
#define MESSAGE_H

#include <QString>
#include <QMetaType>

class Message
{
    QString m_text;
    QString m_imageFilePath;

public:
    Message(QString text = "", QString imageFilePath = "");

    QString getText();
    QString getImageFilePath() { return m_imageFilePath; }

    void setText(QString text);
    void setImageFilePath(QString imageFilePath);
};

Q_DECLARE_METATYPE(Message);

#endif // MESSAGE_H
