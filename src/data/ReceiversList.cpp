#include "ReceiversList.h"

#include <QFile>
#include <QMessageBox>
#include <QTextStream>
#include <QDateTime>

ReceiversList::ReceiversList() :
    QList<Receiver*>()
{

}

bool ReceiversList::loadFromFile(QString filePath)
{
    QMessageBox messageBox;

    QFile file(filePath);
    if (!file.exists())
    {
        messageBox.setText(QString("File % does not exist").arg(filePath));
        messageBox.exec();
        return false;
    }

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        messageBox.setText(QString("Cannot open file %1").arg(filePath));
        messageBox.exec();
        return false;
    }

    this->deleteAllAndClear();

    QTextStream in(&file);
    while (!in.atEnd()) {
        QString receiverLine = in.readLine();

        if (receiverLine.size()!=0)
        {
            Receiver* receiver = new Receiver(receiverLine);
            this->append(receiver);
        }
    }

    return true;
}

void ReceiversList::deleteAllAndClear()
{
    for (int i=0; i<this->size(); ++i)
    {
        delete this->at(i);
    }

    this->clear();
}

bool ReceiversList::checkAllReceivers()
{
    bool res = true;
    m_errorsList.clear();

    for (int i=0; i<this->size(); ++i)
    {
        Receiver* currentReceiver = this->at(i);
        if (!currentReceiver->checkIfAccountExists())
        {
            m_errorsList.append(QString("%1: %2 - %3")
                                .arg(currentReceiver->id())
                                .arg(QDateTime::currentDateTime().toString())
                                .arg(currentReceiver->getLastError()));
            res = false;
        }
    }

    return res;
}

ReceiversList ReceiversList::getReceiversWithAccount()
{
    ReceiversList result;

    for (int i=0; i<this->size(); ++i)
    {
        if (this->at(i)->hasiMessageAccount())
        {
            result.append(this->at(i));
        }
    }

    return result;
}

bool ReceiversList::saveToFile(QString filePath)
{
    QFile file(filePath);
    if (!file.open(QIODevice::ReadWrite | QIODevice::Text | QIODevice::Truncate))
    {
        QMessageBox messageBox;
        messageBox.setText("Cannot open file to write: " + filePath);
        messageBox.exec();
        return false;
    }

    QTextStream out(&file);
    for (int i=0; i<this->size(); ++i)
    {
        Receiver* currentReceiver = this->at(i);
        out << currentReceiver->id() << endl;
    }

    return true;

    /*
    for (int i=0; i < p_proxyTableModel->rowCount(); ++i)
    {
        out << p_proxyTableModel->item(i,0)->text()
            << ":"
            << p_proxyTableModel->item(i,1)->text();

        if (    (p_proxyTableModel->item(i,2)->text().trimmed().size()!=0)
             && (p_proxyTableModel->item(i,3)->text().trimmed().size()!=0) )
        {
            out << ":"
                << p_proxyTableModel->item(i,2)->text()
                << ":"
                << p_proxyTableModel->item(i,3)->text();
        }
        out << "\n";
    }
    */
}
