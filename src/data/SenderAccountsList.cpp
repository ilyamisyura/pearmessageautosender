#include "SenderAccountsList.h"
#include "SenderAccount.h"

#include "processing/QAppleScripter.h"

#include <QString>
#include <QStringList>
#include <QMessageBox>
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QEventLoop>

SenderAccountsList::SenderAccountsList()
{

}

bool SenderAccountsList::loadFromFile(QString filePath)
{

    bool result = true;

    QMessageBox messageBox;

    QFile file(filePath);
    if (!file.exists())
    {
        messageBox.setText(QString("File % does not exist").arg(filePath));
        messageBox.exec();
        return false;
    }

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        messageBox.setText(QString("Cannot open file %1").arg(filePath));
        messageBox.exec();
        return false;
    }

    this->deleteAllAndClear();

    QTextStream in(&file);
    while (!in.atEnd()) {
        QString senderAccountLine = in.readLine();
        QStringList senderAccountList = senderAccountLine.split(':');

        if (senderAccountList.size() == 2)
        {
            SenderAccount* sender = new SenderAccount(senderAccountList[0],senderAccountList[1]);
            this->append(sender);
        }
    }

    return result;
}

void SenderAccountsList::deleteAllAndClear()
{
    for (int i=0; i<this->size(); ++i)
    {
        delete this->at(i);
    }

    this->clear();
}

void SenderAccountsList::logOutFromMessages()
{
    QString logOutScript =
        "activate application \"Messages\"\n"
        "delay 1.1\n"

        "tell application \"System Events\" to tell process \"Messages\"\n"
            "click menu item 3 of menu 2 of menu bar 1\n"
            "delay 1.1\n"
            "click button 2 of toolbar 1 of front window\n"
            "delay 1.1\n"
            "select row 1 of table 1 of scroll area 1 of group 1 of front window\n"
            "delay 1.1\n"
            "click button 2 of tab group 1 of group 1 of front window\n"
            "delay 1.1\n"
            "keystroke return\n"
        "end tell\n";
    QAppleScripter scripter;
    scripter.runAppleScript(logOutScript);
}

void SenderAccountsList::logInRandomAccount()
{
    if (this->size() < 1)
    {
        return;
    }

    logOutFromMessages();

    int chosenIndex = qrand() % (this->size());
    qDebug() << chosenIndex;
    SenderAccount* chosenSender = this->at(chosenIndex);

    if (chosenSender->logIn())
    {
//        emit loggedIn();
    }
}

QString SenderAccountsList::getCurrentAccountName()
{
    QString script =
            "using terms from application \"Messages\"\n"
            "tell application \"Messages\" to get name of 1st service whose service type = iMessage\n"
            "end using terms from\n";


    qDebug() << script;
    QAppleScripter scripter;
    scripter.runAppleScript(script);
}
