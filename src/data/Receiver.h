#ifndef RECEIVER_H
#define RECEIVER_H

#include <QString>
#include <QMetaType>

class Receiver
{
    QString m_id;
    bool m_hasiMessageAccount;
    bool m_lastCheckSucceess;
    bool m_everBeenChecked;

    QString m_lastError;

public:
    Receiver(QString id = "");

    void setId(QString id);
    void setForceHasAccount();
    void setNeedsCheck();

    QString id() { return m_id; }
    bool checkIfAccountExists();

    bool hasiMessageAccount() { return m_hasiMessageAccount; }
    bool getLastCheckSuccess() { return m_lastCheckSucceess; }
    bool everBeenChecked() { return m_everBeenChecked; }

    QString getLastError() { return m_lastError; }
};

Q_DECLARE_METATYPE(Receiver*);

#endif // RECEIVER_H
