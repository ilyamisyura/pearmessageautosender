#ifndef PROXYINFOLIST_H
#define PROXYINFOLIST_H

#include "ProxyInfo.h"

#include <QList>

class ProxyInfoList : public QList<ProxyInfo*>
{
public:
    ProxyInfoList();

    bool loadFromFile(QString filePath);

    void deleteAllAndClear();
};

#endif // PROXYINFOLIST_H
