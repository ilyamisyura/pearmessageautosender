#ifndef PROXYINFO_H
#define PROXYINFO_H

#include <QString>
#include <QMetaType>

class ProxyInfo
{

    QString m_address;
    quint16 m_port;
    QString m_username;
    QString m_password;

public:
    ProxyInfo();
    ProxyInfo(QString address, quint16 port, QString username = "", QString password = "");

    void setAddress(QString address);
    void setPort(quint16 port);
    void setUsername(QString username);
    void setPassword(QString password);

    QString address()           { return m_address; }
    quint16 port()              { return m_port; }
    QString portAsString()      { return QString::number(m_port); }
    QString username()          { return m_username; }
    QString password()          { return m_password; }
};

Q_DECLARE_METATYPE(ProxyInfo*);

#endif // PROXYINFO_H
