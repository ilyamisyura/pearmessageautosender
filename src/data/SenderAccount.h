#ifndef SENDERACCOUNT_H
#define SENDERACCOUNT_H

#include <QMetaType>
#include <QString>

class SenderAccount
{

    QString m_id;
    QString m_password;

public:
    SenderAccount(QString id = "", QString password = "");

    void setId(QString id);
    void setPassword(QString password);

    bool logIn(quint32 waitForLoginSeconds = 30);

    QString id() { return m_id; }
    QString password() { return m_password; }
};

Q_DECLARE_METATYPE(SenderAccount*);

#endif // SENDERACCOUNT_H
