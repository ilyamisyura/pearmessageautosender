#include "SenderAccount.h"
#include "processing/QAppleScripter.h"
SenderAccount::SenderAccount(QString id, QString password)
{
    setId(id);
    setPassword(password);
}

void SenderAccount::setId(QString id)
{
    m_id = id;
}

void SenderAccount::setPassword(QString password)
{
    m_password = password;
}

bool SenderAccount::logIn(quint32 waitForLoginSeconds)
{
    QString script = QString(
            "activate application \"Messages\"\n"
            "delay 1\n"

            "tell application \"System Events\" to tell process \"Messages\"\n"
                "click menu item 3 of menu 2 of menu bar 1\n"
                "delay 1\n"
                "click button 2 of toolbar 1 of front window\n"
                "delay 1\n"
                "click text field 1 of group 1 of group 1 of front window\n"
                "delay 1\n"
                "keystroke \"a\" using command down\n"
                "delay 1\n"
//                "keystroke \"" + m_id + "\"\n"
                "keystroke \"%1\"\n"
                "delay 1\n"
                "keystroke tab\n"
                "delay 1\n"
//                "keystroke \"" + m_password.toLocal8Bit() + "\"\n"
//                "keystroke \"%2\"\n"
                "set user_password to \"%2\"\n"
                "repeat with the_character in user_password\n"
                    "keystroke the_character\n"
                    "delay (random number from 0.1 to 0.5)\n"
                "end repeat\n"
                "click button 2 of group 1 of group 1 of front window\n"
                "set loginResult to \"\"\n"
                "set accountEnabled to false\n"
                "repeat while ((not accountEnabled) and (tries < maxTries))\n"
                    "tell application \"Messages\" to set accountEnabled to enabled of 1st service whose service type = iMessage\n"
                    "set tries to tries + 1\n"
                    "delay 1\n"
                "end repeat\n"

              "select row 1 of table 1 of scroll area 1 of group 1 of front window\n"
              "delay 1\n"

                "if ((tries = maxTries) and (accountEnabled = false)) then\n"
                    "set loginResult to \"error: login timeout\"\n"
                "else\n"
                    "click button 1 of front window\n"
                "end if\n"
                "loginResult\n"

            "end tell\n").arg(m_id).arg(m_password);
    QAppleScripter scripter;
    scripter.runAppleScript(script);
    QString result = scripter.getLastResult();
    return !result.contains("error",Qt::CaseInsensitive);
}
