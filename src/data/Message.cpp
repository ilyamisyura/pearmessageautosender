#include "Message.h"

#include <QStringList>

#include <qmath.h>
#include <random>
#include <QTime>

Message::Message(QString text, QString imageFilePath)
{
    setText(text);
    setImageFilePath(imageFilePath);
}

void Message::setText(QString text)
{
    m_text = text;
}

void Message::setImageFilePath(QString imageFilePath)
{
    m_imageFilePath = imageFilePath;
}

QString Message::getText()
{
    QString result = m_text;

    bool exitCondition = false;
    while (!exitCondition)
    {
        int openBrIndex, closeBrIndex;
        openBrIndex = result.indexOf('{');
        if (openBrIndex >= 0)
        {
            closeBrIndex = result.indexOf('}',openBrIndex);
            if (closeBrIndex >= 0)
            {
                if (closeBrIndex-openBrIndex != 1)
                {
                    QString partForRandomization = result.mid(openBrIndex+1, closeBrIndex-openBrIndex-1);
                    QStringList parts = partForRandomization.split(";;");

                    int chosenPartIndex = qrand() % (parts.size());
                    result.remove(openBrIndex, closeBrIndex-openBrIndex + 1);
                    result.insert(openBrIndex,parts.at(chosenPartIndex));
                }
            }
        }
        else
        {
            exitCondition = true;
        }
    }

    return result;
}
