#ifndef RECEIVERSLIST_H
#define RECEIVERSLIST_H

#include "Receiver.h"

#include <QList>

class ReceiversList : public QList<Receiver*>
{
    QStringList m_errorsList;

public:
    ReceiversList();

    bool loadFromFile(QString filePath);
    bool saveToFile(QString filePath);

    void deleteAllAndClear();

    bool checkAllReceivers();
    ReceiversList getReceiversWithAccount();
};

#endif // RECEIVERSLIST_H
