#include "Receiver.h"
#include "processing/QAppleScripter.h"
#include "Screenshot.h"

#include <QDebug>
#include <QImage>

Receiver::Receiver(QString id)
{
    this->m_id = id;
    this->m_hasiMessageAccount = false;
    this->m_lastCheckSucceess = false;
    this->m_everBeenChecked = false;
}

bool Receiver::checkIfAccountExists()
{
    QAppleScripter scripter;

    //0.5 second delays are needed for correct keystroke input
    QString checkScript =
            QString("activate application \"Messages\"\n"
            "tell application \"System Events\" to tell process \"Messages\"\n"
                "click menu item 1 of menu 3 of menu bar 1\n"
                "delay 1\n"
                "keystroke \"%1\"\n"
                "keystroke return\n"
                "delay 1\n"
                "set coordinates to position of menu button 1 of text field 1 of scroll area 3 of splitter group 1 of window 1\n"
            "end tell\n").arg(QString::fromStdString(this->m_id.toLatin1().toStdString()))
            ;

    this->m_everBeenChecked = true;

    if (!scripter.runAppleScript(checkScript))
    {
        m_lastError = "Error while executing checking script";
        return false;
    }
    QString coordinates = scripter.getLastResult();

    if (coordinates.size()==0)
    {
        m_lastError = "No coordinates received from checking script";
        return false;
    }

    QStringList splittedCoordinates = coordinates.split(',',QString::SplitBehavior::SkipEmptyParts);
    bool xToIntRes;
    int xCoord = QVariant(QString(splittedCoordinates.at(0)).trimmed()).toInt(&xToIntRes) + 5;
    bool yToIntRes;
    int yCoord = QVariant(QString(splittedCoordinates.at(1)).trimmed()).toInt(&yToIntRes) + 5;

    if (!(xToIntRes && yToIntRes))
    {
        m_lastError = "Incorrect coordinates format received";
        return false;
    }

    Screenshot screenshot;
    screenshot.shoot();

    QImage screenshotImage = screenshot.getData().toImage();
    QColor accountColor = screenshotImage.pixelColor(xCoord, yCoord);

    if (accountColor.red() > 200)
    {
        this->m_hasiMessageAccount = false;
    }
    else
    {
        this->m_hasiMessageAccount = true;
    }

    QString closeDialogScript =
            "activate application \"Messages\"\n"
            "delay 1\n"
            "tell application \"System Events\" to tell process \"Messages\"\n"
                "key code 51 using command down\n"
            "end tell\n"
            ;
    scripter.runAppleScript(closeDialogScript);

    this->m_lastCheckSucceess = true;
    return true;
}

void Receiver::setForceHasAccount()
{
    m_hasiMessageAccount = true;
    m_everBeenChecked = true;
    m_lastCheckSucceess = true;
}

void Receiver::setNeedsCheck()
{
    m_hasiMessageAccount = false;
    m_everBeenChecked = false;
    m_lastCheckSucceess = false;
}
