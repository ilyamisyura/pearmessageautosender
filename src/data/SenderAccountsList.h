#ifndef SENDERACCOUNTSLIST_H
#define SENDERACCOUNTSLIST_H

#include <QList>
#include <QObject>

#include "SenderAccount.h"

class SenderAccountsList : public QList<SenderAccount*>
{   
public:
    SenderAccountsList();

    bool loadFromFile(QString filePath);

    void logOutFromMessages();
    void logInRandomAccount();
    QString getCurrentAccountName();

    void deleteAllAndClear();
};

#endif // SENDERACCOUNTSLIST_H
