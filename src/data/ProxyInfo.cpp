#include "ProxyInfo.h"

ProxyInfo::ProxyInfo()
{
}

ProxyInfo::ProxyInfo(QString address, quint16 port, QString username, QString password)
{
    setAddress(address);
    setPort(port);
    setUsername(username);
    setPassword(password);
}

void ProxyInfo::setAddress(QString address)
{
    m_address = address.trimmed();
}

void ProxyInfo::setPort(quint16 port)
{
    m_port = port;
}

void ProxyInfo::setUsername(QString username)
{
    m_username = username.trimmed();
}

void ProxyInfo::setPassword(QString password)
{
    m_password = password.trimmed();
}
