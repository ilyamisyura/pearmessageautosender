#include "ProxyInfoList.h"

#include <QFile>
#include <QMessageBox>
#include <QTextStream>
#include <QDateTime>

ProxyInfoList::ProxyInfoList()
{

}

bool ProxyInfoList::loadFromFile(QString filePath)
{
    QMessageBox messageBox;

    QFile file(filePath);
    if (!file.exists())
    {
        messageBox.setText(QString("File % does not exist").arg(filePath));
        messageBox.exec();
        return false;
    }

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        messageBox.setText(QString("Cannot open file %1").arg(filePath));
        messageBox.exec();
        return false;
    }

    this->deleteAllAndClear();

    QTextStream in(&file);
    while (!in.atEnd()) {
        QString proxyInfoLine = in.readLine();
        QStringList proxyInfoSplittedList = proxyInfoLine.split(':');

        int splittedListSize = proxyInfoSplittedList.size();

        if (splittedListSize > 0)
        {
            ProxyInfo* proxyInfo = new ProxyInfo();
            proxyInfo->setAddress(proxyInfoSplittedList.at(0));
            if (splittedListSize > 1)
            {
                QString portString = proxyInfoSplittedList.at(1);
                proxyInfo->setPort(portString.toInt());
            }
            if (splittedListSize > 2)
            {
                proxyInfo->setUsername(proxyInfoSplittedList.at(2));
            }
            if (splittedListSize > 3)
            {
                proxyInfo->setPassword(proxyInfoSplittedList.at(3));
            }

            this->append(proxyInfo);
        }
    }

    return true;
}

void ProxyInfoList::deleteAllAndClear()
{
    for (int i=0; i<this->size(); ++i)
    {
        delete this->at(i);
    }

    this->clear();
}
