#-------------------------------------------------
#
# Project created by QtCreator 2016-12-12T12:02:08
#
#-------------------------------------------------
include(common.pri)
include(app.pri)

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = "iMessage Sender"
TEMPLATE = app

INCLUDEPATH+= ./src


SOURCES += $${SOURCE_PATH}/main.cpp\
        $${SOURCE_PATH}/ui/MainWindow.cpp \
    $${SOURCE_PATH}/processing/QSystemProxySetupMachine.cpp \
    $${SOURCE_PATH}/ui/MessageEditor.cpp \
    src/processing/QAppleScripter.cpp \
    src/processing/QAppleBasher.cpp \
    src/data/Receiver.cpp \
    src/processing/QiMessageSender.cpp \
    src/data/Screenshot.cpp \
    src/data/ReceiversList.cpp \
    src/data/Message.cpp \
    src/ui/tools/SpinBoxDelegate.cpp \
    src/data/SenderAccount.cpp \
    src/data/SenderAccountsList.cpp \
    src/ui/MessageEditorContainer.cpp \
    src/data/ProxyInfo.cpp \
    src/data/ProxyInfoList.cpp

HEADERS  += $${SOURCE_PATH}/ui/MainWindow.h \
    $${SOURCE_PATH}/processing/QSystemProxySetupMachine.h \
    $${SOURCE_PATH}/ui/MessageEditor.h \
    src/processing/QAppleScripter.h \
    src/processing/QAppleBasher.h \
    src/data/Receiver.h \
    src/processing/QiMessageSender.h \
    src/data/Screenshot.h \
    src/data/ReceiversList.h \
    src/data/Message.h \
    src/ui/tools/SpinBoxDelegate.h \
    src/data/SenderAccount.h \
    src/data/SenderAccountsList.h \
    src/ui/MessageEditorContainer.h \
    src/data/ProxyInfo.h \
    src/data/ProxyInfoList.h

FORMS    += $${SOURCE_PATH}/ui/MainWindow.ui \
    $${SOURCE_PATH}/ui/MessageEditor.ui

RESOURCES += \
    resources/images.qrc
